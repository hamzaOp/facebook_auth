var express = require('express');

const passport = require('passport');
const Strategy = require('passport-facebook').Strategy;
const querystring = require('querystring');
const user = {};

passport.use(
  new Strategy(
    {
      clientID: 'ID',
      clientSecret: 'SECRET',
      callbackURL: 'http://localhost:3001/login/facebook/return'
    },
    function(accessToken, refreshToken, profile, cb) {
      user.accessToken = accessToken;
      user.displayName = profile.displayName;
      user.id = profile.id;
      return cb(null, user);
    }
  )
);

// Create a new Express application.
var app = express();

// Use application-level middleware for common functionality
app.use(require('morgan')('dev'));

// Initialize Passport
app.use(passport.initialize());

// Define routes.
app.get('/', function(req, res) {
  res.json({ user: req.user });
});

app.get('/test', function(req, res) {
  res.json({
    hello: 'world'
  });
});

app.get(
  '/login/facebook',
  passport.authenticate('facebook', {
    session: false
  })
);

app.get(
  '/login/facebook/return',
  passport.authenticate('facebook', {
    session: false
  }),
  function(req, res) {
    res.redirect(
      'http://localhost:3000/feed?' + querystring.stringify(req.user)
    );
  }
);

// listen on port 3001
app.listen(3001);
